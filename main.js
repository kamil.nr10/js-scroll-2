document.body.style.height = "10000px";


let grow = true;
let size = 10;

const div = document.createElement('div');
document.body.appendChild(div);
div.style.width = "100%";
div.style.height = `${size}px`;
div.style.backgroundColor = "green";
div.style.position = "fixed";
div.style.top = "0";
div.style.left = "0";

const changeHeight = () => {
    if (size >= window.innerHeight / 2) {
        grow = !grow
    } else if (size <= 0) {
        grow = !grow
    }

    if (grow) {
        size += 10;
        div.style.backgroundColor = "green";
    } else {
        size -= 10;
        div.style.backgroundColor = "red";
    }
    div.style.height = `${size}px`;

}


window.addEventListener('scroll', changeHeight)